import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { MovieCollectionComponent } from './movie-collection/movie-collection.component';


const routes: Routes = [
  { path: '', loadChildren: './search/search.module#ShopsPageModule' },
  { path: 'collection', loadChildren: './movie-collection/movie-collection.module#MovieCollectionModule' },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
