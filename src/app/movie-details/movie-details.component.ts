import { Component, OnInit, Inject, NgModule } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Movies } from '../models/movies.model';
import { MoviesDatabase } from '../services/movies-database.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  private movie: Movies = <Movies>{};
  private rate:number=0;
  private rateMsg: string;
  private collectionMsg: string;
  constructor(public dialogRef: MatDialogRef<MovieDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data:Movies,
    public movieService:MoviesDatabase) {
      
    movieService.getExtraMovieDetails(this.data.id).then((data:any)=>{     
      this.movie=data; 
      this.movie.budget = data.budget;
      this.movie.revenue = data.revenue;
      this.movie.spoken_languages = data.spoken_languages;
    })
    
   }

  ngOnInit() {

  }
  async submitRate(){
    await this.movieService.rateMovie(this.movie.id,this.rate).then(data=>{
      console.log('Rate Submit');
      this.rateMsg = 'Rate Submit';
    }).catch(err=>{
      console.log(err);
      
    })
  }

  closeDialog(){
    this.dialogRef.close();
  }
  addToCollection(){
    this.movieService.setToLocalStorage(this.movie);
    this.collectionMsg = this.movie.title +', has beed added to collection';
  }
}
