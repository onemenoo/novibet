import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search.component';
import { MatIconModule, MatFormFieldModule, MatInputModule, MatTableModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MovieDetailsComponent } from '../movie-details/movie-details.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSliderModule} from '@angular/material/slider';
@NgModule({
	providers: [
	],
	imports: [
		CommonModule,
		FormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatInputModule,
		ReactiveFormsModule,
		FormsModule,
		ReactiveFormsModule,
		MatTableModule,
		MatPaginatorModule,
		MatDialogModule,
		MatGridListModule,
		MatSliderModule,
		RouterModule.forChild([
			{
				path: '',
				component: SearchComponent
			}
		]),
	],
	declarations: [SearchComponent, MovieDetailsComponent],
	entryComponents: [MovieDetailsComponent]
})
export class ShopsPageModule { }