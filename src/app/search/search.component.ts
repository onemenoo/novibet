import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MoviesDatabase } from '../services/movies-database.service';
import { Movies } from '../models/movies.model';
import { PageEvent, MatDialog } from '@angular/material';
import { MovieDetailsComponent } from '../movie-details/movie-details.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  private searchForm: FormGroup;
  private submitted: boolean = false;
  private pageLength: number;
  private pageEvent: PageEvent;
  private searched: string;
  displayedColumns: string[] = ['poster_path', 'title', 'vote_average'];

  private movies: Movies[];
  constructor(public appComp: AppComponent,
    private fb: FormBuilder,
    public movieService: MoviesDatabase,
    public dialog: MatDialog,
    private route: ActivatedRoute) {
    this.appComp.changeButton('collection');
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {      
      if (params['index'] >= 0 ) {
        this.getRecord(this.movieService.getFromLocalStorageSpesificMovie(params['index']));
      }
    });
    this.searchForm = this.fb.group({
      'search': new FormControl('', Validators.compose([Validators.required, Validators.minLength(3), this.alphanumericCheck]))
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.searchForm.controls; }

  alphanumericCheck(data: AbstractControl): { [key: string]: boolean } | null {
    let patern = /^[a-zA-Z0-9 ]*$/;
    if (data.value.match(patern)) {
      //console.log(data.value.match(letterNumber));
      return null;
    } else {
      return { 'patern': false };
    }
  }

  async onSubmit(value: any, event: PageEvent = null) {
    this.submitted = true;
    if (!this.f.search.errors) {
      await this.movieService.searchMovies(value.search, event ? event.pageIndex + 1 : null)
        .then((data: any) => {
          this.searched = value.search;
          this.pageLength = <number>data.total_pages;
          this.movies = <Movies[]>data.results;
        })
        .catch(err => {
          console.log(err);
        })
    }
  }

  getRecord(data: any) {
    const dialogRef = this.dialog.open(MovieDetailsComponent, {
      width: '1000px',
      data: data
    });
  }
}
