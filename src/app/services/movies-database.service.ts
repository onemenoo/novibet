import { Injectable } from '@angular/core';
import { Auth } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promise } from 'q';
import { environment } from 'src/environments/environment';
import { Movies } from '../models/movies.model';

@Injectable({
	providedIn: 'root'
})
export class MoviesDatabase {
	private sessionData: any;

	constructor(private auth: Auth,private http: HttpClient){
		this.sessionData = this.auth.getGuestSessionID();
	}

	searchMovies(query:string,page:number=1){
		return Promise((resolve,reject)=>{
			this.http.get('https://api.themoviedb.org/3/search/movie?api_key='+environment.api_key+'&query='+query+'&page='+page)
			.subscribe((data:any)=>{
				resolve(data)
			},
			err=>{
				reject(err)
			})
		});
	}
	getExtraMovieDetails(id:number){
		return Promise((resolve,reject)=>{
			this.http.get('https://api.themoviedb.org/3/movie/'+id+'?api_key='+environment.api_key)
			.subscribe((data:any)=>{
				resolve(data)
			},
			err=>{
				reject(err)
			})
		});
	}
	setToLocalStorage(data:Movies){
		let movies:Movies[] = JSON.parse(localStorage.getItem('myCollection')) || [];
		movies.push(data);
		localStorage.setItem('myCollection', JSON.stringify(movies));
	}

	getFromLocalStorage(){
		return JSON.parse(localStorage.getItem('myCollection'));
	}

	getFromLocalStorageSpesificMovie(index:number){
		let collection = JSON.parse(localStorage.getItem('myCollection'));
		return collection[index];
	}

	deleteFromLocalStorage(index:number){
		let movies:Movies[] = JSON.parse(localStorage.getItem('myCollection'));
		movies.splice(index,1);
		localStorage.setItem('myCollection', JSON.stringify(movies));
	}

	rateMovie(id:number, rateNumber:any){
		return Promise((resolve,reject)=>{
			console.log(this.sessionData.guest_session_id);
			
			let headers = new HttpHeaders();
			headers = headers.set('Content-Type', 'application/json; charset=utf-8');
			this.http.post('https://api.themoviedb.org/3/movie/'+id+'/rating?api_key='+environment.api_key+"&guest_session_id="+this.sessionData.guest_session_id,
			{value: rateNumber},
			{headers: headers})
			.subscribe((data:any)=>{
				resolve(data)
			},
			err=>{
				reject(err)
			})
		});
	}
}