import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Promise } from 'q';
import { environment } from 'src/environments/environment';
@Injectable({
	providedIn: 'root'
})
export class Auth {
	private guest_session_id: any;
	
	constructor(private http: HttpClient){

	}

	setGuestSessionID(): Promise<any> {
		return Promise((resolve,reject)=>{
			this.http.get('https://api.themoviedb.org/3/authentication/guest_session/new?api_key='+environment.api_key).subscribe((data:any)=>{
				this.guest_session_id = data;
				resolve(true);
			},err=>{
				console.log(err);
				reject(false);
			})
		})
	}

	getGuestSessionID() {
		return this.guest_session_id;
	}
}