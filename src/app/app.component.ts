import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  title = 'Angular Interview Project';
  private buttonRoute = 'collection';
  private buttonTitle = 'My Collection';

  constructor(){
    
  }
  
  changeButton(action: string = ''){
    if(action == 'collection'){
      this.buttonRoute = 'collection';
      this.buttonTitle = 'My Collection';
    } else {
      this.buttonRoute = '/';
      this.buttonTitle = 'Back';
    }
  }
  
}
