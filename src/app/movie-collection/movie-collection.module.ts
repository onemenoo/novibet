import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieCollectionComponent } from './movie-collection.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material';
@NgModule({
	providers: [
		],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatGridListModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: '',
				component: MovieCollectionComponent
			}
		]),
	],
	declarations: [MovieCollectionComponent]
})
export class MovieCollectionModule { }