import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Movies } from '../models/movies.model';
import { MoviesDatabase } from '../services/movies-database.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-collection',
  templateUrl: './movie-collection.component.html',
  styleUrls: ['./movie-collection.component.scss']
})
export class MovieCollectionComponent implements OnInit {
  private myCollection: Movies[]=[];
  constructor(public appComp:AppComponent,
    public movieService:MoviesDatabase,
    private router: Router) { 
    this.appComp.changeButton();
      this.myCollection = this.movieService.getFromLocalStorage();
  }

  ngOnInit() {
  }

  loadMovie(index){
    this.router.navigate(['/'], { queryParams: { index: index } });
  }

  deleteRow(index:number){
    this.movieService.deleteFromLocalStorage(index);
    this.myCollection = this.movieService.getFromLocalStorage();
  }
}
